This is the old and unmaintained repository for the website of Northeastern
University's chapter of the Society of Asian Scientists and Engineers (SASE).
It runs on the CodeIgniter framework and the Foundation 5 CSS styles.

Check out NUSASE on campus or at [northeatern.edu/sase](http://www.northeastern.edu/sase/).
